package fr.laptoff.inviteplugin.comands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.laptoff.inviteplugin.Managers.PlayerManager;

public class StatsCommand implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args) {

        if(!(sender instanceof Player)){
            sender.sendMessage("§4[ERREUR] Vous devez être un joueur pour pouvoir executer cette commande !");
            return false;
        }

        final Player player = (Player)sender;
        final PlayerManager pm = new PlayerManager(player.getUniqueId());
        String playerName = pm.getPlayerName();
        String playerNation = pm.getPlayerNation();
        String playerNationRank = pm.getPlayerNationRank();
        int playerDeath = pm.getPlayerDeaths();
        int playerKills = pm.getPlayerKillsJoueurs();

        if (pm.getPlayerNation() == "None") {
            playerNation = "Sans Nation";
            playerNationRank = "Sans Grade";
        }

        player.sendMessage("//////////Stats//////////");
        player.sendMessage("Nom du joueur: " + playerName);
        player.sendMessage("Nation du joueur: " + playerNation);
        player.sendMessage("Grade de nation: " + playerNationRank);
        player.sendMessage("Nombre de morts: " + playerDeath);
        player.sendMessage("Nombre de kills: " + playerKills);

        return false;
    }
    
}
