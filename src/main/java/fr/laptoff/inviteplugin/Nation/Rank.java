package fr.laptoff.inviteplugin.Nation;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;

import fr.laptoff.inviteplugin.Managers.DataManager;

public class Rank {

    private final static DataManager dmRank = new DataManager("Player.yml");
    private final static DataManager dmPermissions = new DataManager("Permissions.yml");
    private final static FileConfiguration configuration = dmRank.getFileConfiguration();
    private final static FileConfiguration configurationPerms = dmPermissions.getFileConfiguration();
    private static Map<String, List<String>> nationsRanksMap = new HashMap<String, List<String>>();
    private static List<String> nationList = Nation.getNationList();
    private static List<String> permsList;

    public static void addNationRank(String nation, String rank){
        List<String> list = getNationRankList(nation);
        list.add(rank);
        configuration.set("Nations.nation" + nation, list);
        try {
            configuration.save(dmRank.getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        updateNationsRanksMap();
    }

    public static void addNationPermToRank(String nation, String rank, String perm){
        permsList = configurationPerms.getStringList("Nations." + nation + "." + rank);
        permsList.add(perm);
        configurationPerms.set("Nations." + nation + "." + rank, permsList);
        try {
            configurationPerms.save(dmPermissions.getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void delNationPermToRank(String nation, String rank, String perm){
        permsList = configurationPerms.getStringList("Nations." + nation + "." + rank);
        int i = 0;
        while (i < permsList.size()) {
            if (permsList.get(i) == perm) {
                permsList.remove(i);
            }
        }
        configurationPerms.set("Nations." + nation + "." + rank, permsList);
        try {
            configurationPerms.save(dmPermissions.getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<String> getNationRanksPerm(String nation, String rank){
        return configurationPerms.getStringList("Nations." + nation + "." + rank);
    }
    
    public static void delNationRank(String nation, String rank){
        List<String> list = getNationRankList(nation);
        int i = 0;
        while(i < list.size()){
            if (list.get(i) == rank) {
                list.remove(i);
            }
        }
        configuration.set("Nations.nation" + nation, list);
        try {
            configuration.save(dmRank.getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        updateNationsRanksMap();
    }

    public static void changeNationRank(String nation, String rank, String newRank){
        delNationRank(nation, rank);
        addNationRank(nation, newRank);
    }

    public static List<String> getNationRankList(String nation){
        return nationsRanksMap.get(nation);
    }

    public static void updateNationsRanksMap(){
        for (String s : nationList) {
            nationsRanksMap.replace(s, configuration.getStringList(s));
        }
    }

    public static void updateNationsRanksConfiguration(){
        for (String s : nationList) {
            configuration.set("Nations.nation" + s, nationsRanksMap.get(s));
        }
        try {
            configuration.save(dmRank.getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
