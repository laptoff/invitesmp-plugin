package fr.laptoff.inviteplugin.Nation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.configuration.file.FileConfiguration;

import fr.laptoff.inviteplugin.Managers.DataManager;
import fr.laptoff.inviteplugin.Managers.PlayerManager;

public class Nation {

    private String Name;
    private static PlayerManager pm;
    private static final DataManager dmNation = new DataManager("Nation.yml");
    private static final DataManager dmNationMembers = new DataManager("Nation_Members.yml");
    private static final File fileNation = dmNation.getFile();
    private static final File fileMembers = dmNationMembers.getFile();
    private static final FileConfiguration configurationNation = dmNation.getFileConfiguration();
    private static final FileConfiguration configurationMembers = dmNationMembers.getFileConfiguration();
    private static List<String> listNameNation = new ArrayList<String>();
    private static Map<String, List<UUID>> listNationMembers = new HashMap<String, List<UUID>>();

    public Nation(String name, UUID leaderUuid){
        this.Name = name;
        if (!(isExist(name))) {
            listNameNation.add(name);
            addNationMember(leaderUuid, name, "Leader");
            Rank.addNationRank(name, "Leader");
            Rank.addNationRank(name, "Ministre");
            Rank.addNationRank(name, "Citoyen");
            Rank.addNationRank(name, "Recrue");
            updateNationConfigurationList();
        }
        updateNationList();
    }

    public String getName(){
        return this.Name;
    }

    public void changeName(String s){
        int i = 0;
        for (String name : listNameNation) {
            if(name == getName()){
                listNameNation.set(i, s);
                updateNationConfigurationList();
            }
            i++;
        }
    }

    public void addNationMember(UUID uuid, String nationName, String rank){
        pm = new PlayerManager(uuid);
        pm.setPlayerNation(nationName);
        pm.setPlayerRank(rank);
        List<UUID> list = getMembersList(nationName);
        list.add(uuid);
        listNationMembers.replace(nationName, list);
        updateMembersConfigurationList();
    }

    public List<UUID> getMembersList(String name){
        return listNationMembers.get(name);
    }

    public static void updateMembersList(){
        for (String s : listNameNation) {
            List<String> list = configurationMembers.getStringList(s);
            List<UUID> list_ = new ArrayList<UUID>();

            for (String member : list) {
                list_.add(UUID.fromString(member));
            }

            listNationMembers.replace(s, list_);
        }
    }

    public static void updateMembersConfigurationList(){
        for (String s : listNameNation) {
            List<UUID> uuidPlayer = listNationMembers.get(s);
            configurationMembers.set(s, uuidPlayer);
        }
        try {
            configurationMembers.save(fileMembers);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<String> getNationList(){
        return listNameNation;
    }

    public void addToNationList(String name){
        if (!(isExist(name))) {
            listNameNation.add(name);
        }
        updateNationConfigurationList();
    }

    public void delToNationList(String name){
        int i = 0;
        while (i >= listNameNation.size()) {
            if (listNameNation.get(i) == name) {
                listNameNation.remove(i);
            }
        }
        updateNationConfigurationList();
    }

    public static void updateNationList(){
        listNameNation = configurationNation.getStringList("Nations");
    }

    public static void updateNationConfigurationList(){
        configurationNation.set("Nations", listNameNation);
        try {
            configurationNation.save(fileNation);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Boolean isExist(String name){

        updateNationList();
        for (String s : listNameNation) {
            if (s == name) {
                return true;
            }
        }
        return false;
    }
    
}
