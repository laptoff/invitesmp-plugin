package fr.laptoff.inviteplugin.Managers;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import fr.laptoff.inviteplugin.Plugin;

public class DataManager {

    private Plugin plugin = Plugin.getInstance();
    private String dataFolder = "Data/";
    private File file;
    private FileConfiguration configFile; 

    public DataManager(String filePath){
        this.file = new File(plugin.getDataFolder() + "/" + dataFolder + filePath);

        if (!(file.exists())) {
            file.getParentFile().mkdirs();
            plugin.saveResource(dataFolder + filePath, false);
        }

        configFile = new YamlConfiguration();

        try {
            configFile.load(file);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

     public FileConfiguration getFileConfiguration(){
        return this.configFile;
    }

    public File getFile(){
        return this.file;
    }
}
