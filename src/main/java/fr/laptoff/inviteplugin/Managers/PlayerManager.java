package fr.laptoff.inviteplugin.Managers;

import fr.laptoff.inviteplugin.Nation.Rank;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class PlayerManager {

    private UUID Uuid;
    private Player player;
    private static final DataManager dmPlayer = new DataManager("Player.yml");
    private static final FileConfiguration configuration = dmPlayer.getFileConfiguration();
    private static String key;
    

    public PlayerManager(UUID uuid){

        this.Uuid = uuid;
        this.player = Bukkit.getPlayer(uuid);
        key = "Joueurs." + this.Uuid.toString();

    }


    public String getPlayerName() {
        return configuration.getString(key + ".Name");
    }

    public void setplayerName(String name){
        configuration.set(key + ".Name", name);
        player.setDisplayName(name);
        try {
            configuration.save(dmPlayer.getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getPlayerNation(){
        return configuration.getString(key + ".Nation");
    }

    public void setPlayerNation(String name){
        configuration.set(key + ".Nation", name);
        try {
            configuration.save(dmPlayer.getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getPlayerNationRank(){
        return configuration.getString(key + ".Rank");
    }

    public void setPlayerRank(String name){
        configuration.set(key + ".Rank", name);
        try {
            configuration.save(dmPlayer.getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Boolean hasNationPermission(String perm){
        List<String> list = getPlayerNationPermList();
        for (String string : list) {
            if (string == perm) {
                return true;
            }
        }
        return false;
    }

    public List<String> getPlayerNationPermList(){
        return Rank.getNationRanksPerm(getPlayerNation(), getPlayerNationRank());
    }

    public int getPlayerDeaths(){
        return configuration.getInt(key + ".Morts");
    }

    public void setPlayerDeaths(int num){
        configuration.set(key + ".Morts", num);
        try {
            configuration.save(dmPlayer.getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getPlayerKillsJoueurs(){
        return configuration.getInt(key + ".Kills_Joueurs");
    }

    public void setPlayerKillsJoueurs(int num){
        configuration.set(key + ".Kills_Joueurs", num);
        try {
            configuration.save(dmPlayer.getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
