package fr.laptoff.inviteplugin;

import fr.laptoff.inviteplugin.Managers.DataManager;
import fr.laptoff.inviteplugin.Nation.Nation;
import fr.laptoff.inviteplugin.Nation.Rank;
import fr.laptoff.inviteplugin.comands.PingCommand;
import fr.laptoff.inviteplugin.comands.StatsCommand;
import fr.laptoff.inviteplugin.listeners.playerJoin;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Plugin extends JavaPlugin {

    private static Plugin instance;
    private final static PluginManager pm = Bukkit.getPluginManager();
    private PluginDescriptionFile pdf = getDescription();

    @Override
    public void onEnable() {
        instance = this;
        DataManager dmPlayer = new DataManager("Player.yml");
        DataManager dmRank = new DataManager("Rank.yml");
        DataManager dmPermissions = new DataManager("Permissions.yml");
        DataManager dmNation = new DataManager("Nation.yml");
        DataManager dmNationMembers = new DataManager("Nation_Members.yml");
        Nation.updateNationList();
        Nation.updateMembersList();
        Rank.updateNationsRanksMap();
        

        getLogger().info("Invite !");
        getLogger().info("Le plugin InviteSMP v." + pdf.getVersion() + " a démarré !");
        

        getCommand("ping").setExecutor(new PingCommand());
        getCommand("stats").setExecutor(new StatsCommand());

        pm.registerEvents(new playerJoin(), instance);
    }

    @Override
    public void onDisable() {
        getLogger().info("Invite !");
        System.out.println(" __");
        System.out.println("|");
        System.out.println("|");
        System.out.println(" __");
        System.out.println("   |");
        System.out.println("   |");
        System.out.println(" __");
    }

    public static Plugin getInstance() {
        return instance;
    }
}