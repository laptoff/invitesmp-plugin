package fr.laptoff.inviteplugin.listeners;

import fr.laptoff.inviteplugin.Managers.DataManager;
import fr.laptoff.inviteplugin.Managers.PlayerManager;

import java.util.UUID;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class playerJoin implements Listener {

    private static final DataManager dmPlayer = new DataManager("Player.yml");
    private static final FileConfiguration configuration = dmPlayer.getFileConfiguration();

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {

        final Player player = e.getPlayer();
        final UUID uuid = player.getUniqueId();
        final PlayerManager pm = new PlayerManager(uuid);
        final String key = "Joueurs." + uuid.toString();

        final ConfigurationSection configurationSection = configuration.getConfigurationSection(key);

        if(!(configurationSection == null)) {
            player.sendMessage("§2Rebonjour " + player.getName() + " !");
        } else {
            player.sendMessage("§aBienvenue " + player.getName() + " sur InviteSMP !");
            pm.setplayerName(player.getName());
            pm.setPlayerNation("None");
            pm.setPlayerRank("None");
            pm.setPlayerDeaths(0);
            pm.setPlayerKillsJoueurs(0);
        }
    }
}